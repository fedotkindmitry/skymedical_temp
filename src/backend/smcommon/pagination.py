from rest_framework import pagination
from rest_framework.response import Response


class RemovablePagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'limit'

    def get_page_size(self, request):
        """ Disable pagination by 'listening' for a zero value for page_size_query_param """
        if self.page_size_query_param:
            try:
                return pagination._positive_int(
                    request.query_params[self.page_size_query_param], strict=False, cutoff=self.max_page_size)
            except (KeyError, ValueError):
                pass

        return self.page_size

    def _prepare_paginated_response(self, data):
        return {
            'links': {
                'pagination': {
                    'next_page_url': self.get_next_link(),
                    'prev_page_url': self.get_previous_link(),
                    'total': self.page.paginator.count,
                    'current_page': self.page.number,
                    'from': self.page.start_index(),
                    'to': self.page.end_index(),
                    'per_page': self.page.paginator.per_page,
                    'last_page': self.page.paginator.page_range[-1],
                }
            },
            'results': data
        }

    def get_paginated_response(self, data):
        return Response(self._prepare_paginated_response(data))

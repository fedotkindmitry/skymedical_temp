from rest_framework.permissions import IsAuthenticated


class PublicViewOnlyPermission(IsAuthenticated):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True

        return super().has_permission(request, view)

# Generates the app.js for each portal
#
# It recieves as argument the node command to run for each portal:
# - watch
# - webpack
PORTALS=(frontend admin)

[[ -z "$1" ]] && echo "ERROR: npm command as argument required" && exit 2

fronte() {
    cd src/$1
    if [ "$3" != 'noinstall' ]; then
        echo "$1: $(date +'%F %H:%M:%S') Installing node dependecies..."
        npm install
    fi
    echo "$1: Running: $2"
    npm run $2
}

export -f fronte

if [ "$1" == "watch" ]; then
    # Watch should be run in forked processes, because the processes will never
    # end
    for PORTAL in "${PORTALS[@]}"; do
        fronte $PORTAL watch $2&
    done
    wait
else
    for PORTAL in "${PORTALS[@]}"; do
        (
        # Run in a subshell
        fronte $PORTAL $1 $2
        )
    done
fi

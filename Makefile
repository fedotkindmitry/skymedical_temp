MANAGE=python manage.py
APPS=smcommon
BACKUP_DIR=_backup
ARCHIVE_DIR="${BACKUP_DIR}/archive"

# If the first argument is "test"...
ifeq (test,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "test"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (restoredb,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

reqs:
	@docker-compose exec web bash -c "cd src/backend/ && pip install -r requirements.txt"

deletemigrations:
	@echo "Deleting all pre-existing migrations"
	@./shell_inside_docker.sh "rm -f */migrations/0*"

migrate:
	@echo "Creating missing migrations and migrating all apps"
	@./shell_inside_docker.sh "$(MANAGE) makemigrations && $(MANAGE) migrate"

remigrate:
	@echo "Recreate all migrations and apply them to the existing database without affecting the actual data"
	@$(MAKE) deletemigrations
	@./shell_inside_docker.sh "$(MANAGE) makemigrations $(APPS) && $(MANAGE) migrate --fake-initial"

resetdb:
	@echo "Creating an empty database"
	@docker-compose rm -sf db
	@docker-compose up -d db
	@sleep 3
	@$(MAKE) remigrate

test:
	@echo "Running Django test runner"
	@./shell_inside_docker.sh "./manage.py test $(RUN_ARGS)"

coverage:
	@echo "Calculating coverage"
	@./shell_inside_docker.sh "./calculate_coverage.sh"

reloadnode:
	@echo "Restarting the node container"
	@docker-compose restart -t 0 node

rebuildnode:
	@echo "Rebuilding the node container"
	@docker-compose stop -t 0 node && docker-compose up -d --build node

reload:
	@echo "Reloading all containers"
	@docker-compose stop && docker-compose up -d

rebuild:
	@echo "Rebuilding all containers and seeding db"
	@docker-compose rm -sf
	@docker-compose stop && docker-compose up -d

bash:
	@echo "Starting a bash shell in the Django web container"
	@./shell_inside_docker.sh "bash"

shell:
	@echo "Starting a Django shell in the Django web container"
	@./shell_inside_docker.sh "$(MANAGE) shell"

comparesettings:
	@diff src/backend/env/settings.env src/backend/env/settings.env-template

resetsettings:
	@cp src/backend/env/settings.env-template src/backend/env/settings.env
